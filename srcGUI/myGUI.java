import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class myGUI {
    private JButton btnRed;
    private JButton btnYellow;
    private JButton btnGreen;
    private JButton btnDefault;
    private JButton btnBlue;
    private JButton btnCoseColor;
    private JButton btnArial;
    private JButton btnCosmicSansMS;
    private JButton btnCourierNew;
    private JTextField tfdTextArea;
    private JButton btnWroteInLabel;
    private JButton btnDeleteInLabel;
    private JButton btnTextRed;
    private JButton btnTextBlue;
    private JButton btnTextBlack;
    private JButton btnPlusTextSize;
    private JButton btnMinusTextSize;
    private JButton btnTextLeft;
    private JButton btnTextCenter;
    private JButton btnTextRight;
    private JButton btnExit;
    private JPanel test;
    private JLabel lblText;
    private JLabel lbl1;
    private JTextField tfdField;

    public myGUI() {
        btnRed.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                test.setBackground(Color.red);
            }
        });
        btnGreen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                test.setBackground(Color.green);
            }
        });
        btnBlue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                test.setBackground(Color.blue);
            }
        });
        btnYellow.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                test.setBackground(Color.yellow);
            }
        });
        btnDefault.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                test.setBackground(null);
            }
        });
        btnCoseColor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        btnArial.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tfdField.setFont(new Font("Arial", Font.PLAIN, 14));
            }
        });
        btnCosmicSansMS.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tfdField.setFont(new Font("Comic Sans MS", Font.PLAIN, 14));
            }
        });
        btnCourierNew.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tfdField.setFont(new Font("Courier New", Font.PLAIN, 14));
            }
        });
        btnTextRed.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tfdField.setForeground(Color.red);
            }
        });
        btnTextBlue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tfdField.setForeground(Color.blue);
            }
        });
        btnTextBlack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tfdField.setForeground(Color.black);
            }
        });
        btnTextLeft.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tfdField.setHorizontalAlignment(SwingConstants.LEFT);
            }
        });
        btnTextCenter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tfdField.setHorizontalAlignment(SwingConstants.CENTER);
            }
        });
        btnTextRight.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tfdField.setHorizontalAlignment(SwingConstants.RIGHT);
            }
        });
        btnPlusTextSize.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int size = tfdField.getFont().getSize();
                tfdField.setFont(new Font(tfdTextArea.getFont().getName(), Font.PLAIN, ++size));
            }
        });
        btnMinusTextSize.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int size = tfdField.getFont().getSize();
                tfdField.setFont(new Font(tfdTextArea.getFont().getName(), Font.PLAIN, --size));
            }
        });
        btnWroteInLabel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tfdField.setText(tfdTextArea.getText());
            }
        });
        btnDeleteInLabel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tfdField.setText(null);
            }
        });
        btnExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        btnCoseColor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Color ausgewaehlteFarbe = JColorChooser.showDialog(null,
                        "Farbauswahl", null);
                test.setBackground(ausgewaehlteFarbe);
            }
        });
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("myGUI");
        frame.setContentPane(new myGUI().test);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
