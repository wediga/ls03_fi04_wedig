package model.persistance;

import model.User;

public interface IUserPersistance {

    User readUser(String username);

}