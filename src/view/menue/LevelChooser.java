package view.menue;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import model.Level;
import model.User;
import view.game.GameGUI;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

    private LevelController lvlControl;
    private LeveldesignWindow leveldesignWindow;
    private AlienDefenceController alienDefenceController;
    private User user;
    private JTable tblLevels;
    private DefaultTableModel jTableData;

    /**
     * Create the panel.
     *
     * @param leveldesignWindow
     * @param alienDefenceController
     * @param user
     * @param herkunft
     */
    public LevelChooser(LeveldesignWindow leveldesignWindow, AlienDefenceController alienDefenceController, User user, String herkunft) {
        this.lvlControl = alienDefenceController.getLevelController();
        this.leveldesignWindow = leveldesignWindow;
        this.alienDefenceController = alienDefenceController;
        this.user = user;

        setLayout(new BorderLayout());


        JPanel pnlButtons = new JPanel();
        pnlButtons.setBackground(Color.BLACK);
        add(pnlButtons, BorderLayout.SOUTH);

        if (herkunft.equals("spielen")) {
            JButton btnTesten = new JButton("Spielen");
            btnTesten.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    btnTesten_Clicked();
                }
            });
            pnlButtons.add(btnTesten);
        } else if (herkunft.equals("testen")) {
            JButton btnTesten = new JButton("Testen");
            btnTesten.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    btnTesten_Clicked();
                }
            });
            pnlButtons.add(btnTesten);
        } else {

            JButton btnNewLevel = new JButton("Neues Level");
            btnNewLevel.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    btnNewLevel_Clicked();
                }
            });
            pnlButtons.add(btnNewLevel);

            JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
            btnUpdateLevel.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    btnUpdateLevel_Clicked();
                }
            });
            pnlButtons.add(btnUpdateLevel);

            JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
            btnDeleteLevel.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    btnDeleteLevel_Clicked();
                }
            });
            pnlButtons.add(btnDeleteLevel);
        }

        JLabel lblLevelauswahl = new JLabel("Levelauswahl");
        lblLevelauswahl.setFont(new Font("Arial", Font.BOLD, 18));
        lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
        lblLevelauswahl.setForeground(Color.GREEN);
        add(lblLevelauswahl, BorderLayout.NORTH);

        JScrollPane spnLevels = new JScrollPane();
        add(spnLevels, BorderLayout.CENTER);

        tblLevels = new JTable();
        tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblLevels.setForeground(Color.ORANGE);
        tblLevels.setFillsViewportHeight(true);
        tblLevels.setBackground(Color.BLACK);
        tblLevels.getTableHeader().setForeground(Color.ORANGE);
        tblLevels.getTableHeader().setBackground(Color.BLACK);
        spnLevels.setViewportView(tblLevels);

        this.updateTableData();
    }

    private String[][] getLevelsAsTableModel() {
        List<Level> levels = this.lvlControl.readAllLevels();
        String[][] result = new String[levels.size()][];
        int i = 0;
        for (Level l : levels) {
            result[i++] = l.getData();
        }
        return result;
    }

    public void updateTableData() {
        this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
        this.tblLevels.setModel(jTableData);
    }

    public void btnNewLevel_Clicked() {
        this.leveldesignWindow.startLevelEditor();
    }

    public void btnUpdateLevel_Clicked() {
        int level_id = Integer
                .parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
        this.leveldesignWindow.startLevelEditor(level_id);
    }

    public void btnDeleteLevel_Clicked() {
        int level_id = Integer
                .parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
        this.lvlControl.deleteLevel(level_id);
        this.updateTableData();
    }

    private void btnTesten_Clicked() {
        Thread t = new Thread("GameThread") {
            @Override
            public void run() {
                GameController gameController = alienDefenceController.startGame(alienDefenceController.getLevelController().readAllLevels().get(0), user);
                new GameGUI(gameController).start();
            }
        };
        t.start();
    }
}
