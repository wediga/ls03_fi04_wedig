package view.menue;

import controller.AlienDefenceController;
import controller.LevelController;
import model.Level;
import model.User;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

@SuppressWarnings("serial")
public class LeveldesignWindow extends JFrame {

    private LevelController lvlControl;
    private JPanel contentPane;
    private LevelChooser cardChooseLevel;
    private LevelEditor cardLevelEditor;

    private CardLayout cards;

    /**
     * Create the frame.
     *
     * @param alienDefenceController
     * @param user
     */
    public LeveldesignWindow(AlienDefenceController alienDefenceController, User user, String spielen) {
        this.lvlControl = alienDefenceController.getLevelController();

        setTitle("Leveldesigner");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        this.cards = new CardLayout();
        contentPane.setBackground(Color.BLACK);
        contentPane.setLayout(cards);

        this.cardChooseLevel = new LevelChooser(this, alienDefenceController, user, spielen);
        cardChooseLevel.setBackground(Color.BLACK);
        contentPane.add(cardChooseLevel, "levelChooser");
// todo clean up code
        this.cardLevelEditor = new LevelEditor(this, lvlControl, alienDefenceController.getTargetController(), Level.getDefaultLevel());
        contentPane.add(cardLevelEditor, "levelEditor");

        this.showLevelChooser();
        this.setVisible(true);
    }

    /**
     * display leveleditor with a new level
     */
    public void startLevelEditor() {
        this.cardLevelEditor.setLvl(this.lvlControl.createLevel());
        this.cards.show(contentPane, "levelEditor");
    }

    /**
     * disply leveleditor with a new level
     *
     * @param level_id
     */
    public void startLevelEditor(int level_id) {
        this.cardLevelEditor.setLvl(this.lvlControl.readLevel(level_id));
        this.cards.show(contentPane, "levelEditor");
    }

    /**
     * display a jTable with all Levels
     */
    public void showLevelChooser() {
        this.cards.show(contentPane, "levelChooser");
        this.cardChooseLevel.updateTableData();
    }

}
